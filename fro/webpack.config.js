const path = require('path')

const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const name = path.basename(path.join(__dirname, '..'))
const dist = path.join(__dirname, 'dist')
module.exports = {
	target: 'web',
	mode: 'none',
	entry: path.join(__dirname, 'index.js'),
	output: {
		path: dist,
		filename: `${name}.js`
	},
	devtool: "source-map",
	resolve: {
		alias: {svelte: path.resolve('node_modules', 'svelte')},
		extensions: ['.mjs', '.js', '.svelte'],
		mainFields: ['svelte', 'browser', 'module', 'main']
	},
	module: {
		rules: [{
			test: /\.(html|svelte)$/,
			use: {
				loader: 'svelte-loader',
				options: {
					onwarn: (warning, onwarn)=>{
						if(warning.message.match(/A11y/)) return
						onwarn(warning)
					}
				}
			}
		}]
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: name.replace('-', ' ').replace(/\b\w/g, c=> c.toUpperCase()),
			favicon: 'asset/favicon.png',
			meta: {viewport: 'width=device-width, shrink-to-fit=yes'}
		}),
		new webpack.DefinePlugin({
			'process.env.SERVER': JSON.stringify(process.env.SERVER || `http://localhost:${process.env.PORT||80}`)
		})
	]
}
